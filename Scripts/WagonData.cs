﻿
namespace WpfAppTest.Scripts
{
    public class WagonData
    {
        public WagonData(int TotalCount, int AmountInSpeedTrain, int AmountInPassengerTrain, int Passengers)
        {
            this.TotalCount = TotalCount;
            this.AmountInSpeedTrain = AmountInSpeedTrain;
            this.AmountInPassengerTrain = AmountInPassengerTrain;
            this.Passengers = Passengers;
        }

        public int TotalCount;
        public int Passengers;

        public int AmountInSpeedTrain;
        public int AmountInPassengerTrain;

        //total passangers in speed train
        public int TotalInSpeedTrain() { return Passengers * AmountInSpeedTrain; }

        //total passangers in passanger train
        public int TotalInPassengerTrain() { return Passengers * AmountInPassengerTrain; }

        public static WagonData Parse(string s1, string s2, string s3, string s4)
        {
            return new WagonData(int.Parse(s1), int.Parse(s2), int.Parse(s3), int.Parse(s4));
        }
    }
}
