﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WpfAppTest.Scripts
{
    internal class Gomori
    {
        TrainStationData newData;
        int fastTrains = 0;
        int passTrains = 0;


        /// <summary>
        /// Ця функція приймає на вхід матрицю коефіцієнтів A, вектор b, вектор c і індекси базисних змінних B.
        /// </summary>
        public static double[] SolveWithHomoriMethod(double[,] A, double[] b, double[] c, int[] B)
        {
            int m = b.Length; // кількість обмежень
            int n = c.Length; // кількість змінних

            // Додаємо штучних змінних
            int s = m;
            double[,] newA = new double[m, n + m];
            double[] newc = new double[n + m];
            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    newA[i, j] = A[i, j];
                }
                newA[i, n + i] = 1;
                newc[n + i] = -1;
            }

            // Перевіряємо, чи всі b >= 0
            for (int i = 0; i < m; i++)
            {
                if (b[i] < 0)
                {
                    for (int j = 0; j < n + m; j++)
                    {
                        newA[i, j] *= -1;
                    }
                    b[i] *= -1;
                }
            }

            // Знаходимо початковий базис
            int[] B0 = new int[m];
            for (int i = 0; i < m; i++)
            {
                B0[i] = n + i;
            }

            // Знаходимо опорний вектор
            double[] x0 = new double[n + m];
            double[] y = new double[m];
            double v = 0;
            for (int i = 0; i < m; i++)
            {
                y[i] = b[i];
            }

            // Розв'язуємо задачу оптимізації за допомогою симплекс-методу
            Tuple<double, double[]> simplexResult = new Simplex.Simplex(c, A, b).maximize();
            x0 = simplexResult.Item2;
            v = simplexResult.Item1;

            // Якщо значення цільової функції від'ємне, то задача нерозв'язна
            if (v < 0)
            {
                throw new Exception("The problem is infeasible.");
            }

            // Поки є штучні змінні в базисі
            while (B0.Any(lmd => lmd >= n))
            {
                int e = Array.FindIndex(B0, lmd2 => lmd2 >= n); // індекс штучної змінної в базисі
                int j = -1;
                for (int i = 0; i < n; i++)
                {
                    if (Math.Abs(newA[e, i]) > 1e-8)
                    {
                        j = i;
                        break;
                    }
                }
                if (j == -1)
                {
                    throw new Exception("Cannot find non-zero coefficient in artificial variable row.");
                }

                // Знаходимо вектор, що допоможе вивести штучну змінну з базису
                double[] d = new double[n + m];
                d[j] = 1;
                for (int i = 0; i < m; i++)
                {
                    if (i == e) continue;
                    double k = -newA[i, j] / newA[e, j];
                    for (int l = 0; l < n + m; l++)
                    {
                        newA[i, l] += k * newA[e, l];
                    }
                    y[i] += k * y[e];
                }
                B0[e] = j;

                // Розв'язуємо задачу оптимізації за допомогою симплекс-методу
                simplexResult = new Simplex.Simplex(c, A, b).maximize();
                x0 = simplexResult.Item2;
                v = simplexResult.Item1;
            }

            // Видаляємо штучні змінні з базису
            int[] finalBasis = new int[m];
            int cnt = 0;
            for (int i = 0; i < m; i++)
            {
                if (B0[i] < n)
                {
                    finalBasis[cnt++] = B0[i];
                }
            }

            // Розв'язуємо задачу оптимізації за допомогою симплекс-методу
            //simplexResult = Simplex(A, b, c, finalBasis); //original
            simplexResult = new Simplex.Simplex(c, A, b).maximize();

            double[] x = simplexResult.Item2;
            double vFinal = simplexResult.Item1;

            // Перевіряємо коректність розв'язку
            for (int i = 0; i < m; i++)
            {
                double lhs = 0;
                for (int j = 0; j < n; j++)
                {
                    lhs += A[i, j] * x[j];
                }
                if (lhs > b[i] + 1e-8)
                {
                    throw new Exception("The final solution is incorrect.");
                }
            }

            return x;
        }





        public Tuple<double, double[]> Solve(TrainStationData trainStationData, Tuple<double, double[]> simplexData)
        {
            int passengersTransported = (int)(simplexData.Item1);

            double t1 = simplexData.Item2[0];
            fastTrains = (int)Math.Truncate(t1);

            double t2 = simplexData.Item2[1];
            passTrains = (int)Math.Truncate(t2);


            newData = GetUnusedWagons(trainStationData, fastTrains, passTrains);



            while (newData.CanAssembleFastTrain(fastTrains) || newData.CanAssemblePassengerTrain(passTrains))
            {
                //fast train
                if (newData.IsFastTrainBetter())
                {
                    if (!AssembeFast())
                        AssembePass();
                } //pass train
                else
                {
                    if (!AssembePass())
                        AssembeFast();
                }
            }


            int totalPassengers = newData.PassengersInFastTrain() * fastTrains + newData.PassengersInPassTrain() * passTrains;
            return Tuple.Create<double, double[]>(totalPassengers, new double[] { fastTrains, passTrains });
        }

        bool AssembeFast()
        {
            if (newData.CanAssembleFastTrain(fastTrains))
            {
                newData.AssembleFastTrain();
                fastTrains++;
                return true;
            }
            return false;
        }
        bool AssembePass()
        {
            if (newData.CanAssemblePassengerTrain(passTrains))
            {
                newData.AssemblePassTrain();
                passTrains++;
                return true;
            }
            return false;
        }

        public TrainStationData GetUnusedWagons(TrainStationData data, int fastTrains, int passTrains)
        {
            var returnData = new TrainStationData(data);

            returnData.LuggageWagon.TotalCount -= data.LuggageWagon.AmountInSpeedTrain * fastTrains;
            returnData.LuggageWagon.TotalCount -= data.LuggageWagon.AmountInPassengerTrain * passTrains;

            returnData.PostWagon.TotalCount -= data.PostWagon.AmountInSpeedTrain * fastTrains;
            returnData.PostWagon.TotalCount -= data.PostWagon.AmountInPassengerTrain * passTrains;

            returnData.HardWagon.TotalCount -= data.HardWagon.AmountInSpeedTrain * fastTrains;
            returnData.HardWagon.TotalCount -= data.HardWagon.AmountInPassengerTrain * passTrains;

            returnData.KupeWagon.TotalCount -= data.KupeWagon.AmountInSpeedTrain * fastTrains;
            returnData.KupeWagon.TotalCount -= data.KupeWagon.AmountInPassengerTrain * passTrains;

            returnData.SoftWagon.TotalCount -= data.SoftWagon.AmountInSpeedTrain * fastTrains;
            returnData.SoftWagon.TotalCount -= data.SoftWagon.AmountInPassengerTrain * passTrains;



            return returnData;
        }

    }
}
