﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfAppTest.Scripts
{
    public class TrainStationData
    {
        public WagonData LuggageWagon;
        public WagonData PostWagon;
        public WagonData HardWagon;
        public WagonData KupeWagon;
        public WagonData SoftWagon;

        public bool IsFastConstaintsActive = false;
        public int MaxFastTrains = 0;

        public bool IsPassConstaintsActive = false;
        public int MaxPassengerTrains = 0;

        public TrainStationData()
        { }

        public TrainStationData(TrainStationData data)
        {
            this.LuggageWagon = data.LuggageWagon;
            this.PostWagon = data.PostWagon;
            this.HardWagon = data.HardWagon;
            this.KupeWagon = data.KupeWagon;
            this.SoftWagon = data.SoftWagon;

            this.IsFastConstaintsActive = data.IsFastConstaintsActive;
            this.MaxFastTrains = data.MaxFastTrains;

            this.IsPassConstaintsActive = data.IsPassConstaintsActive;
            this.MaxPassengerTrains = data.MaxPassengerTrains;

        }

        public bool CanAssembleFastTrain(int fastTrainsAssembled)
        {
            bool res = LuggageWagon.TotalCount >= LuggageWagon.AmountInSpeedTrain &&
            PostWagon.TotalCount >= PostWagon.AmountInSpeedTrain &&
            HardWagon.TotalCount >= HardWagon.AmountInSpeedTrain &&
            KupeWagon.TotalCount >= KupeWagon.AmountInSpeedTrain &&
            SoftWagon.TotalCount >= SoftWagon.AmountInSpeedTrain;

            if (IsFastConstaintsActive)
                res = res && fastTrainsAssembled < MaxFastTrains;

            return res;
        }

        public bool CanAssemblePassengerTrain(int passTrainsAssembled)
        {
            bool res = LuggageWagon.TotalCount >= LuggageWagon.AmountInPassengerTrain &&
            PostWagon.TotalCount >= PostWagon.AmountInPassengerTrain &&
            HardWagon.TotalCount >= HardWagon.AmountInPassengerTrain &&
            KupeWagon.TotalCount >= KupeWagon.AmountInPassengerTrain &&
            SoftWagon.TotalCount >= SoftWagon.AmountInPassengerTrain;

            if (IsPassConstaintsActive)
                res = res && (passTrainsAssembled < MaxPassengerTrains);

            return res;
        }


        public int PassengersInFastTrain()
        {
            return LuggageWagon.TotalInSpeedTrain() + PostWagon.TotalInSpeedTrain() +
                HardWagon.TotalInSpeedTrain() + KupeWagon.TotalInSpeedTrain() +
                SoftWagon.TotalInSpeedTrain();
        }

        public int PassengersInPassTrain()
        {
            return LuggageWagon.TotalInPassengerTrain() + PostWagon.TotalInPassengerTrain() +
                HardWagon.TotalInPassengerTrain() + KupeWagon.TotalInPassengerTrain() +
                SoftWagon.TotalInPassengerTrain();
        }


        public void AssembleFastTrain()
        {
            LuggageWagon.TotalCount -= LuggageWagon.AmountInSpeedTrain;
            PostWagon.TotalCount -= PostWagon.AmountInSpeedTrain;
            HardWagon.TotalCount -= HardWagon.AmountInSpeedTrain;
            KupeWagon.TotalCount -= KupeWagon.AmountInSpeedTrain;
            SoftWagon.TotalCount -= SoftWagon.AmountInSpeedTrain;
        }

        public void AssemblePassTrain()
        {
            LuggageWagon.TotalCount -= LuggageWagon.AmountInPassengerTrain;
            PostWagon.TotalCount -= PostWagon.AmountInPassengerTrain;
            HardWagon.TotalCount -= HardWagon.AmountInPassengerTrain;
            KupeWagon.TotalCount -= KupeWagon.AmountInPassengerTrain;
            SoftWagon.TotalCount -= SoftWagon.AmountInPassengerTrain;
        }



        public bool IsFastTrainBetter()
        {
            return PassengersInFastTrain() > PassengersInPassTrain();
        }
    }
}
