using System;
using System.Diagnostics;
using System.Collections.Generic;

namespace Simplex {
  class Simplex {
    private double[] c;
    private double[,] A;
    private double[] b;
    private HashSet<int> N = new HashSet<int>();
    private HashSet<int> B = new HashSet<int>();
    private double v = 0;

    public Simplex(double[] c, double[,] A, double[] b) {
      int vars = c.Length, constraints = b.Length;

        //System.Console.WriteLine(String.Join(", ", c));
        //for (int i = 0; i < A.GetLength(0); i++)
        //{
        //    for (int j = 0; j < A.GetLength(1); j++)
        //    {
        //        System.Console.WriteLine(A[i, j] + "\t");
        //    }
        //    System.Console.WriteLine();

        //}
        //System.Console.WriteLine(String.Join(", ",b));



            if (vars != A.GetLength(1)) {
        throw new Exception("ʳ������ ������ � c �� ������� ����� � A.");
      }

      if (constraints != A.GetLength(0)) {
        throw new Exception("ʳ������ �������� � A �� ������� ����� � b.");
      }

      // ��������� ������������ ������ ����������� fn �� ����������� 0
      this.c = new double[vars+constraints];
      Array.Copy(c, this.c, vars);

      // ��������� ������� ����������� �� ����������� 0
      this.A = new double[vars+constraints,vars+constraints];
      for (int i = 0; i < constraints; i++) {
        for (int j = 0; j < vars; j++) {
          this.A[i+vars, j] = A[i,j];
        }
      }

      // Extend constraint right-hand side vector with 0 padding
      // ������ ������ ��������� ���������� � ����������� 0
      this.b = new double[vars+constraints];
      Array.Copy(b, 0, this.b, vars, constraints);

      // Populate non-basic and basic sets
      // ��������� ��������� �� ������� ������
      for (int i = 0; i < vars; i++) {
        N.Add(i);
      }

      for (int i = 0; i < constraints; i++) {
        B.Add(vars + i);
      }
            //System.Console.WriteLine(String.Join(", ", this.c));
            //for (int i = 0; i < this.A.GetLength(0); i++)
            //{
            //    for (int j = 0; j < this.A.GetLength(1); j++)
            //    {
            //        System.Console.WriteLine(this.A[i, j] + "\t");
            //    }
            //    System.Console.WriteLine();

            //}
            //System.Console.WriteLine(String.Join(", ", this.b));
            //System.Console.WriteLine(String.Join(", ", this.N));
            //System.Console.WriteLine(String.Join(", ", this.B));
        }
        

        /// <summary>
        /// ������� Tuple, ���� ������ ����������� �������� ������� �� ����� ������, 
        /// �� ���������� ������������ ����'����.
        /// </summary>
        /// <returns></returns>
        public Tuple<double, double[]> maximize() {
      while (true) {
        // Find highest coefficient for entering var
        // ������ �������� ���������� ��� �������� �����
        int e = -1;
        double ce = 0;
        foreach (var _e in N) {
          if (c[_e] > ce) {
            ce = c[_e];
            e = _e;
          }
        }

        // ���� ����� ���������� �� > 0, ����� �� ������� ������������
        if (e == -1) break;

        // ������� ��������� ���������� ��������
        double minRatio = double.PositiveInfinity;
        int l = -1;
        foreach (var i in B) {
          System.Console.WriteLine(A[i, e]);
          if (A[i, e] > 0) {
            double r = b[i] / A[i, e];
            if (r < minRatio) {
              minRatio = r;
              l = i;
            }
          }
        }

        // Unbounded
        if (double.IsInfinity(minRatio)) {
          return Tuple.Create<double, double[]>(double.PositiveInfinity, null);
        }

        pivot(e, l);
      }


      // ��������� ���� �� ����� ��� ������������ ������
      double[] x = new double[b.Length];
      int n = b.Length;
      for (var i = 0; i < n; i++) {
        x[i] = B.Contains(i) ? b[i] : 0;
      }

      // ��������� �������� � �����
      return Tuple.Create<double, double[]>(v, x);
    }

        /// <summary>
        /// ����� pivot ������ ������� �� ������ ������ ������ ����� ������ �
        /// ����� �� �������, � ����� ���������� ����� �����������.
        /// </summary>
        /// <param name="e"></param>
        /// <param name="l"></param>
        private void pivot(int e, int l) {
      N.Remove(e);
      B.Remove(l);

      b[e] = b[l] / A[l, e];

      foreach (var j in N) {
        A[e, j] = A[l, j] / A[l, e];
      }

      A[e, l] = 1 / A[l, e];

      foreach (var i in B) {
        b[i] = b[i] - A[i, e] * b[e];

        foreach (var j in N) {
          A[i, j] = A[i, j] - A[i, e] * A[e, j];
        }

        A[i, l] = -1 * A[i, e] * A[e, l];
      }

      v = v + c[e] * b[e];

      foreach (var j in N) {
        c[j] = c[j] - c[e] * A[e, j];
      }

      c[l] = -1 * c[e] * A[e, l];

      N.Add(l);
      B.Add(e);
    }
  }

    /*
      var s = new Simplex(
        new [] {10.2, 422.3, 6.91, 853},
        new [,] {
          {0.1, 0.5, 0.333333, 1},
          {30, 15, 19, 12},
          {1000, 6000, 4100, 9100},
          {50, 20, 21, 10},
          {4, 6, 19, 30}
        },
        new double[] {2000, 1000, 1000000, 640, 432}
      );

      var answer = s.maximize();
      Console.WriteLine(answer.Item1);
      Console.WriteLine(string.Join(", ", answer.Item2));
    */
}
