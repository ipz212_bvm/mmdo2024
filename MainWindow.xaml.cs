﻿using Extreme.Mathematics.Optimization;
using Simplex;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfAppTest.Scripts;


namespace WpfAppTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            LabelAnswer1.Content = "";
            LabelAnswer2.Content = "";

            SetDanger(false);
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }



        private void OnContraintsChecked(object sender, RoutedEventArgs e)
        {
            Constraints_Group.IsEnabled = true;
        }

        private void OnContraintsUnchecked(object sender, RoutedEventArgs e)
        {
            Constraints_Group.IsEnabled = false;
        }

        private void ButtonCalculate_Click(object sender, RoutedEventArgs e)
        {
            //MethodSimplex2();
            CollectData();

            if (trainStation.LuggageWagon.Passengers == 0 && trainStation.PostWagon.Passengers == 0 &&
                trainStation.KupeWagon.Passengers == 0 && trainStation.HardWagon.Passengers == 0
                && trainStation.SoftWagon.Passengers == 0)
            {
                SetDanger(true);
            }
            else
            {
                SetDanger(false);
                MethodHomori();
            }
        }

        void SetDanger(bool state)
        {
            if (state)
            {
                labelDanger.Visibility = Visibility.Visible;
                LabelAnswer1.Visibility = Visibility.Hidden;
                LabelAnswer2.Visibility = Visibility.Hidden;
            }
            else
            {
                labelDanger.Visibility = Visibility.Hidden;
                LabelAnswer1.Visibility = Visibility.Visible;
                LabelAnswer2.Visibility = Visibility.Visible;
            }
        }

        private void OnWindowLoaded(object sender, RoutedEventArgs e)
        {
            Constraints_Group.IsEnabled = true;
        }


        private void MethodLinear()
        {
            //LinearProgram lp1 = new LinearProgram(1, 2, 3, 5);
        }

        TrainStationData trainStation;

        void CollectData()
        {
            trainStation = new TrainStationData()
            {
                LuggageWagon = WagonData.Parse(labelLuggagePark.Text, labelLuggageTrainS.Text, labelLuggageTrainP.Text, labelLuggagePassengers.Text),
                PostWagon = WagonData.Parse(labelPostPark.Text, labelPostTrainS.Text, labelPostTrainP.Text, labelPostPassengers.Text),
                HardWagon = WagonData.Parse(labelHardPark.Text, labelHardTrainS.Text, labelHardTrainP.Text, labelHardPassengers.Text),
                KupeWagon = WagonData.Parse(labelKupePark.Text, labelKupeTrainS.Text, labelKupeTrainP.Text, labelKupePassengers.Text),
                SoftWagon = WagonData.Parse(labelSoftPark.Text, labelSoftTrainS.Text, labelSoftTrainP.Text, labelSoftPassengers.Text),

                IsPassConstaintsActive = true,
                MaxPassengerTrains = int.Parse(labelMaxPassTrainsConstraint.Text),
            };


        }
        private void MethodHomori()
        {



            //COMPLETE MATRIX
            //top expression part
            double[] MainExpr = new double[]
            {
                // passengers in speed train
                (trainStation.LuggageWagon.TotalInSpeedTrain() + trainStation.PostWagon.TotalInSpeedTrain() +
                trainStation.HardWagon.TotalInSpeedTrain() + trainStation.KupeWagon.TotalInSpeedTrain() +
                trainStation.SoftWagon.TotalInSpeedTrain()),

                // passengers in passengers train
                (trainStation.LuggageWagon.TotalInPassengerTrain() + trainStation.PostWagon.TotalInPassengerTrain() +
                trainStation.HardWagon.TotalInPassengerTrain() + trainStation.KupeWagon.TotalInPassengerTrain() +
                trainStation.SoftWagon.TotalInPassengerTrain())
            };

            //variables part
            double[,] VarsLeftPart = new double[,]
            {

                { trainStation.LuggageWagon.AmountInSpeedTrain, trainStation.LuggageWagon.AmountInPassengerTrain },
                { trainStation.PostWagon.AmountInSpeedTrain, trainStation.PostWagon.AmountInPassengerTrain },
                { trainStation.HardWagon.AmountInSpeedTrain, trainStation.HardWagon.AmountInPassengerTrain },
                { trainStation.KupeWagon.AmountInSpeedTrain, trainStation.KupeWagon.AmountInPassengerTrain },
                { trainStation.SoftWagon.AmountInSpeedTrain, trainStation.SoftWagon.AmountInPassengerTrain },

                { 0, trainStation.IsPassConstaintsActive ? 1 : 0  }, //0x1 + 1x2 <= 6
               //{ 0,1 },
            };
            double[] VarsRightPart = new double[]
            {
                trainStation.LuggageWagon.TotalCount,
                trainStation.PostWagon.TotalCount,
                trainStation.HardWagon.TotalCount,
                trainStation.KupeWagon.TotalCount,
                trainStation.SoftWagon.TotalCount,

                trainStation.IsPassConstaintsActive ? trainStation.MaxPassengerTrains : 0,
                //6
            };

            int[] B = new int[] { 2, 3, 4, 5,6,7,8 };

            //SOLVE
            //double[] answer = Gomori.SolveWithHomoriMethod(VarsLeftPart, VarsRightPart, MainExpr, B);
            //for (int i = 0; i < answer.Length; i++)
            //{
            //    System.Console.WriteLine("Answer GO -- " + answer[i]);
            //}

            Simplex.Simplex SimplexExpr = new Simplex.Simplex(MainExpr, VarsLeftPart, VarsRightPart);
            //Console.WriteLine(String.Join(", ", MainExpr));
            //int rows = VarsLeftPart.GetLength(0);
            //int columns = VarsLeftPart.GetLength(1);
            //for (int i = 0; i < rows; i++)
            //{
            //    for (int j = 0; j < columns; j++)
            //    {
            //       // Console.Write(VarsLeftPart[i, j] + "\t");
            //    }
            //   // Console.WriteLine();
            //}
            var answerSimplex = SimplexExpr.maximize();
            Console.WriteLine($"First Item: {answerSimplex.Item1}");
            Console.WriteLine($"Second Item: [{String.Join(", ", answerSimplex.Item2)}]");

            var answerGomori = new Gomori().Solve(trainStation, answerSimplex);

            LabelAnswer1.Content = $"Відповідь - {answerGomori.Item2[0]} швидких і {answerGomori.Item2[1]} пасажирських поїздів";
            LabelAnswer2.Content = $"Кількість - {answerGomori.Item1} пасажирів";
        }
    }
}
